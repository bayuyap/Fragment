package id.sch.smktelkom_mlg.fragmentbayu.Fragment;

/**
 * Created by SMK TELKOM on 3/1/2018.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.sch.smktelkom_mlg.fragmentbayu.R;

public class KoalaFragment extends Fragment {
    public KoalaFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.activity_multiple_koala, container, false);
        return rootview;
    }


}
