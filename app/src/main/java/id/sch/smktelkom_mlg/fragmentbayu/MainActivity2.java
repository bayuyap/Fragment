package id.sch.smktelkom_mlg.fragmentbayu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import id.sch.smktelkom_mlg.fragmentbayu.Fragment.MultipleFragment;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener {
    Button quit, brk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        quit = findViewById(R.id.Exit);
        brk = findViewById(R.id.nxt);
        quit.setOnClickListener(this);
        brk.setOnClickListener(this);

    }

    public void multiple(View view) {
        Intent intent = new Intent(getApplicationContext(), MultipleFragment.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View clicked) {
        switch (clicked.getId()) {
            case R.id.Exit:
                exit();
                break;
            case R.id.nxt:
                brkt();
                break;
        }
    }

    private void brkt() {
        Intent i = new Intent(getApplicationContext(), MainActivity3.class);
        startActivity(i);
    }

    private void exit() {
        Intent intent = new Intent(MainActivity2.this, MainActivity2.class);
        finish();
        startActivity(intent);
    }

}
