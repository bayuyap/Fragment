package id.sch.smktelkom_mlg.fragmentbayu;

import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity3 extends AppCompatActivity implements View.OnClickListener {
    private ImageButton buttonAlarm;
    private Button btnnext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        buttonAlarm = findViewById(R.id.btnjam);
        btnnext = findViewById(R.id.nxt);
        btnnext.setOnClickListener(this);
        buttonAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAlarm("Waktu Pulang", 16, 30);
            }

            private void createAlarm(String message, int hour, int minutes) {
                Intent alarmIntent = new Intent(AlarmClock.ACTION_SET_ALARM);
                alarmIntent.putExtra(AlarmClock.EXTRA_MESSAGE, message);
                alarmIntent.putExtra(AlarmClock.EXTRA_HOUR, hour);
                alarmIntent.putExtra(AlarmClock.EXTRA_MINUTES, minutes);
                if (alarmIntent.resolveActivity(getPackageManager()) !=
                        null) startActivity(alarmIntent);
            }
        });
    }

    @Override
    public void onClick(View clicked) {
        switch (clicked.getId()) {
            case R.id.Exit:
                exit();
                break;
            case R.id.nxt:
                brkt();
                break;
        }
    }

    private void brkt() {
        Intent i = new Intent(getApplicationContext(), MainActivity4.class);
        startActivity(i);
    }

    private void exit() {
        Intent intent = new Intent(MainActivity3.this, MainActivity3.class);
        finish();
        startActivity(intent);
    }
}
