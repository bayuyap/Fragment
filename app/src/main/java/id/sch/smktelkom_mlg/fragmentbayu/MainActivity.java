package id.sch.smktelkom_mlg.fragmentbayu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button psn, keluar, tplist, next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        psn = findViewById(R.id.toastBtn);
        keluar = findViewById(R.id.exitBtn);
        tplist = findViewById(R.id.listDialogBtn);
        next = findViewById(R.id.nxt);
        psn.setOnClickListener(this);
        keluar.setOnClickListener(this);
        tplist.setOnClickListener(this);
        next.setOnClickListener(this);
        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }
    }

    @Override
    public void onClick(View clicked) {
        switch (clicked.getId()) {
            case R.id.listDialogBtn:
                munculListDialog();
                break;
            case R.id.toastBtn:
                Toast.makeText(this, "Kamu memilih toast",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.exitBtn:
                exit();
                break;
            case R.id.nxt:
                brkt();
                break;

        }
    }

    private void brkt() {
        Intent i = new Intent(getApplicationContext(), MainActivity2.class);
        startActivity(i);
    }

    private void munculListDialog() {
        final CharSequence[] items = {"Programming", "Teknisi", "Designer", "Gamer", "Developer", "Android"};
        AlertDialog.Builder kk = new AlertDialog.Builder(this);
        kk.setTitle("Choose Your Job");
        kk.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                Toast.makeText(getApplicationContext(), items[item],
                        Toast.LENGTH_SHORT).show();
            }

        }).show();
    }

    private void exit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Want To Quit? ").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int id) {
                MainActivity.this.finish();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int argl) {
                dialogInterface.cancel();
            }
        }).show();
    }

}
