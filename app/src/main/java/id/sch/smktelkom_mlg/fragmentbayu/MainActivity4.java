package id.sch.smktelkom_mlg.fragmentbayu;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity4 extends AppCompatActivity implements View.OnClickListener {
    Button quit;
    private ImageButton buttonmaps;
    private TextView Java;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        Java = findViewById(R.id.java_text);
        quit = findViewById(R.id.Exit);
        buttonmaps = findViewById(R.id.buttonmaps);
        quit.setOnClickListener(this);
        buttonmaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri geoLocation = Uri.parse("geo:0,0?q=-6.870563, 107.594126(SMK Telkom Malang)?z=23");
                showMap(geoLocation);
            }

            //method showMap
            private void showMap(Uri geoLocation) {
                Intent mapIntent = new Intent(Intent.ACTION_VIEW);
                mapIntent.setData(geoLocation);
                if (mapIntent.resolveActivity(getPackageManager())
                        != null) startActivity(mapIntent);
            }
        });
        //Ketika TextView Java diklik

        Java.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);

                intent.setData(Uri.parse("https://www.udacity.com/learn/java"));
                startActivity(intent);
            }
        });


    }

    @Override
    public void onClick(View clicked) {
        switch (clicked.getId()) {
            case R.id.Exit:
                exit();
                break;

        }
    }

    private void exit() {
        Intent intent = new Intent(MainActivity4.this, MainActivity4.class);
        finish();
        startActivity(intent);
    }
}
